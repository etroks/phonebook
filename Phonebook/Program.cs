﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneBook
{
    class Program
    {
        public static int id = 1;
        public static Dictionary<int, Contact> contacts = new Dictionary<int, Contact>();
        public static bool work = true;
        public static string strokaVvoda;

        static void Main(string[] args)
        {
            while (work)
            {
                Console.WriteLine("1. Чтобы создать новую запись, нажмите 1\n" +
                    "2. Чтобы редактировать созданную запись, нажмите 2\n" +
                    "3. Чтобы удалить созданную запись, нажмите 3\n" +
                    "4. Чтобы просмотреть конкретную запись, нажмите 4\n" +
                    "5. Чтобы посмотреть все записи, нажмите 5\n" +
                    "6. Чтобы закончить работу, нажмите 6");
                try
                {
                    switch (Int32.Parse(Console.ReadLine()))
                    {
                        case 1:
                            {
                                try
                                {
                                    Console.WriteLine("Введите имя, фамилию, номер и страну контакта, через пробел.");
                                    strokaVvoda = Console.ReadLine();
                                    contacts.Add(id, new Contact(id, strokaVvoda.Split(' ')[0], strokaVvoda.Split(' ')[1],
                                        strokaVvoda.Split(' ')[2], strokaVvoda.Split(' ')[3]));
                                    id++;
                                }
                                catch (FormatException)
                                {
                                    Console.WriteLine("Ошибка ввода, повторите попытку.");
                                }
                                catch (IndexOutOfRangeException)
                                {
                                    Console.WriteLine("Ошибка ввода, повторите попытку.");
                                }
                                break;
                            }

                        case 2:
                            {
                                try
                                {
                                    Console.WriteLine("Введите через пробел:\n" +
                                        "1. id записи, котрую хотите редактировать.\n" +
                                        "2. К какому полю вы хотите обратиться: Фамилия, Имя, Отчество,\n" +
                                        "Номер, Страна, Дата рождения, Органиация, Должность, Прочие заметки.\n" +
                                        "3. Текст, который вы хотите написать в это поле. Еси вы хотите его удалить, напишите - отсутствует.");
                                    strokaVvoda = Console.ReadLine();
                                    Program.Redact(Int32.Parse(strokaVvoda.Split(' ')[0]), strokaVvoda.Split(' ')[1], strokaVvoda.Split(' ')[2]);
                                }
                                catch (FormatException)
                                {
                                    Console.WriteLine("Ошибка ввода, повторите попытку.");
                                }
                                catch (IndexOutOfRangeException)
                                {
                                    Console.WriteLine("Ошибка ввода, повторите попытку.");
                                }
                                break;
                            }

                        case 3:
                            {
                                try
                                {
                                    Console.WriteLine("Введите id записи, котрую хотите удалить");
                                    contacts.Remove(Int32.Parse(Console.ReadLine()));
                                    Console.WriteLine("Запись удалена.");
                                }
                                catch (FormatException)
                                {
                                    Console.WriteLine("Ошибка ввода, повторите попытку.");
                                }
                                break;
                            }

                        case 4:
                            {
                                try
                                {
                                    Console.WriteLine("Введите id записи, котрую хотите посмотреть");
                                    Console.WriteLine(contacts[Int32.Parse(Console.ReadLine())].ToStringFull());
                                }
                                catch (FormatException)
                                {
                                    Console.WriteLine("Ошибка ввода, повторите попытку.");
                                }
                                catch (KeyNotFoundException)
                                {
                                    Console.WriteLine("Записи с этим id не существует.");
                                }
                                break;
                            }

                        case 5:
                            {
                                foreach (KeyValuePair<int, Contact> contact in contacts)
                                {
                                    Console.WriteLine(contact.ToString());
                                }
                                if (contacts.Count == 0)
                                    Console.WriteLine("Контактов нет.");
                                break;
                            }

                        case 6:
                            {
                                Console.WriteLine("Конец работы.");
                                work = false;
                                break;
                            }
                        default:
                            {
                                Console.WriteLine("Ошибка ввода, повторите попытку.");
                                break;
                            }
                    }
                }
                catch (FormatException)
                {
                    Console.WriteLine("Ошибка ввода, повторите попытку.");
                }
            }


        }

        public static void Redact(int id, string pole, string value)
        {
            switch (pole)
            {
                case "Фамилия":
                    {
                        Program.contacts[id].Famil = value;
                        break;
                    }
                case "Имя":
                    {
                        Program.contacts[id].Name = value;
                        break;
                    }
                case "Отчество":
                    {
                        Program.contacts[id].Father = value;
                        break;
                    }
                case "Номер":
                    {
                        Program.contacts[id].PhoneNumber = value;
                        break;
                    }
                case "Страна":
                    {
                        Program.contacts[id].Country = value;
                        break;
                    }
                case "Дата рождения":
                    {
                        Program.contacts[id].Birthday = value;
                        break;
                    }
                case "Организация":
                    {
                        Program.contacts[id].Organisation = value;
                        break;
                    }
                case "Должность":
                    {
                        Program.contacts[id].Work = value;
                        break;
                    }
                case "Прочие заметки":
                    {
                        Program.contacts[id].Another = value;
                        break;
                    }
                default:
                    {
                        Console.WriteLine("Поле введено не правильно, повторите попытку");
                        break;
                    }
            }
        }
    }
}
