﻿namespace PhoneBook
{
    class Contact
    {
        private int id;
        private string name;
        private string famil;
        private string father;
        private string phoneNumber;
        private string country;
        private string birthday;
        private string organisation;
        private string work;
        private string another;

        public string Name { get => name; set => name = value; }
        public string Famil { get => famil; set => famil = value; }
        public string Father { get => father; set => father = value; }
        public string PhoneNumber { get => phoneNumber; set => phoneNumber = value; }
        public string Country { get => country; set => country = value; }
        public string Birthday { get => birthday; set => birthday = value; }
        public string Organisation { get => organisation; set => organisation = value; }
        public string Work { get => work; set => work = value; }
        public string Another { get => another; set => another = value; }
        public int Id { get => id; set => id = value; }

        public Contact(int id, string name, string famil, string phone, string country)
        {
            this.Id = id;
            this.Name = name;
            this.Famil = famil;
            this.PhoneNumber = phone;
            this.Country = country;
        }
        public override string ToString()
        {
            return ("ID: " + Id + ". Фамилия контакта " + Famil + ". Имя контакта " + Name + ". Номер: " + PhoneNumber);
        }
        public string ToStringFull()
        {
            return ("ID: " + Id + ". Фамилия контакта " + Famil + ". Имя контакта " + Name
                + ". Отчество контакта" + (Father ?? " отсутствует") + ". Номер: " +
                PhoneNumber + ". Страна " + Country + ". \nДень рождения " + (Birthday ?? " отсутствует") +
                ". Организация" + (Organisation ?? " отсутствует") +
                ". Должность" + (Work ?? " отсутствует") +
                ". Прочие заметки" + (Another ?? " отсутствуют."));
        }
    }
}
